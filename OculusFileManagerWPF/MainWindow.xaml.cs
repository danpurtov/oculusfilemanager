﻿using MediaDevices;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;

namespace OculusFileManagerWPF
{
  /// <summary>
  /// Логика взаимодействия для MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    private List<string> filesToDelete = new List<string>();
    public MainWindow()
    {
      InitializeComponent();
      picturesList.Items.Add("-- Select Picture --");
      picturesList.SelectedIndex = 0;

    }
    public List<string> GetRecursFiles(string start_path)
    {
      List<string> ls = new List<string>();
      try
      {
        string[] folders = Directory.GetDirectories(start_path);
        foreach (string folder in folders)
        {
          ls.Add("Папка: " + folder);
          ls.AddRange(GetRecursFiles(folder));
        }
      }
      catch (System.Exception e)
      {
        Console.WriteLine(e.Message);
      }
      return ls;
    }

    public void ShowDirectoriesFromUsb(string deviceName)
    {
      DriveInfo[] info = DriveInfo.GetDrives();
      foreach (var item in info)
      {
        if (item.IsReady == true)
        {
          if (item.VolumeLabel != "OS" && item.VolumeLabel != "DATA")
          {
            Console.WriteLine(item.VolumeLabel);
            if (item.VolumeLabel == deviceName)
            {
              List<string> ls = GetRecursFiles(item.Name);
              foreach (string fname in ls)
              {
                  outputText.Text += fname+'\n';
                
              }

            }
          }

        }
      }
    }
    public void PrintDir(string _dir,MediaDevice device)
    {
      string[] allDirs = device.GetDirectories(_dir);
      if (allDirs.Length > 1)
      {
        foreach (var dir in allDirs)
        {
          outputText.Text += dir.Replace(@"\Внутренний общий накопитель\", "") + "\n";
          PrintDir(dir,device);
        }
      }
    }
    public void ShowDirectoriesFromDevice(string deviceName)
    {
      connectedDevices.Text = null;
      IEnumerable<MediaDevice> devices = MediaDevice.GetDevices();
      int count = 0;
      Console.WriteLine("Список подключенных устройств : ");
      foreach (var device in devices)
      {
        device.Connect();
        connectedDevices.Text+=device.Manufacturer+" "+device.Model+"\n";
        if (device.Description == deviceName)
        {
          count++;
          string[] allDirs;
          try
          {
            allDirs = device.GetDirectories(@"\Внутренний общий накопитель");
            if (allDirs.Length > 1)
            {
              foreach (var dir in allDirs)
              {
                outputText.Text += dir.Replace(@"\Внутренний общий накопитель\", "") + "\n";
                PrintDir(dir, device);
              }
            }
            var getFiles = device.GetFiles(@"\Внутренний общий накопитель\DCIM");
          }
          catch (Exception)
          {
            try
            {
              allDirs = device.GetDirectories(@"\Internal Storage\");
              
              foreach (var dir in allDirs)
              {
                outputText.Text += dir.Replace(@"\Internal Storage\", "") + "\n";
                PrintDir(dir, device);
              }
              
              var getFiles = device.GetFiles(@"\Internal Storage\DCIM");
            }
            catch (Exception)
            {
              MessageBox.Show("Выбранное устройство недоступно");
              return;
            }
          }
        }
        device.Disconnect();
      }
      if (count == 0)
      {
        MessageBox.Show("Выбранное устройство недоступно");
      }
    }
    public string GetPath()
    {
      OpenFileDialog dialog = new OpenFileDialog();
      return dialog.ShowDialog() == true ? dialog.FileName : null;
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      string deviceName = inputText.Text;
      ShowDirectoriesFromDevice(deviceName);
      AddPicturesToBox();
    }

    private void UploadPictures(object sender, RoutedEventArgs e)
    {
      label.Content = "";
      string filePath = GetPath();
      if (filePath == "")
      {
        return;
      }
      try
      {
        byte[] bData = File.ReadAllBytes(filePath);

        MemoryStream memory = new System.IO.MemoryStream();
        memory.Write(bData, 0, bData.Length);

        FileInfo fileInf = new FileInfo(filePath);
        if (fileInf.Exists)
        {
          IEnumerable<MediaDevice> devices = MediaDevice.GetDevices();
          foreach (var device in devices)
          {
            if (device.Description == inputText.Text)
            {
              device.Connect();
              string[] allDirs = device.GetDirectories(@"\Внутренний общий накопитель");
              foreach (var dir in allDirs)
              {
                if (dir.Contains("Pictures"))
                {
                FileStream file = File.OpenRead(filePath);
                string[] fileName = file.Name.Split('\\');
                file.Close();
                  label.Content = fileName[fileName.Length - 1];
                string newPictureDir = dir + @"\NewImages\"+fileName[fileName.Length-1];
                file.Close();
                  memory.Position = 0;
                  device.UploadFile(memory, newPictureDir);
                }
              }
              device.Disconnect();
            }
          }
        }
    }
      catch (Exception)
      {
        MessageBox.Show("Не получается загрузить файл");
        return;
      }
}


    private static void WriteSreamToDisk(string filePath, MemoryStream memoryStream)
    {
      try
      {
        string[] path = filePath.Split('\\');
        string picturesTempFolder = "";
        for (int i = 0; i < path.Length - 1; i++)
          picturesTempFolder += path[i]+'\\';
        Directory.CreateDirectory(picturesTempFolder);
        using (FileStream file = new FileStream(filePath, FileMode.Create, System.IO.FileAccess.Write))
        {
          byte[] bytes = new byte[memoryStream.Length];
          _ = memoryStream.Read(bytes, 0, (int)memoryStream.Length);
          
          file.Write(bytes, 0, bytes.Length);
          memoryStream.Close();
        }
      }
      catch(Exception)
      {
        return;
      }
    }
    private void AddPicturesToBox()
    {
      picturesList.Items.Clear();
      picturesList.SelectedIndex = 0;
      var devices = MediaDevice.GetDevices();
      foreach (var device in devices)
      {
        if (device.Description == inputText.Text)
        {
          device.Connect();
          MediaDirectoryInfo photoDir;
          try
          {
            photoDir = device.GetDirectoryInfo(@"\Внутренний общий накопитель\Pictures\NewImages\");
          }
          catch(Exception)
          {
            photoDir = device.GetDirectoryInfo(@"\Internal Storage\");
          }
          string[] files = device.GetFiles(photoDir.FullName);
          
          foreach (var file in files)
          {
            if(file.Replace(photoDir.FullName + @"\", "").Contains(".jpg")|| file.Replace(photoDir.FullName + @"\", "").Contains(".png")||file.Replace(photoDir.FullName + @"\", "").Contains(".bmp")|| file.Replace(photoDir.FullName + @"\", "").Contains(".jpeg"))
              picturesList.Items.Add(file.Replace(photoDir.FullName + @"\", ""));
          }
          device.Disconnect();
        }
      }
    }
    private void Button_Click_1(object sender, RoutedEventArgs e)
    {
      //скачиваю нужную картинку в поток, потом вывожу этот поток во временный файл, оттуда в форму
      var devices = MediaDevice.GetDevices();
      foreach (var device in devices)
      {
        if (device.Description == inputText.Text)
        {
          device.Connect();
          MediaDirectoryInfo photoDir = device.GetDirectoryInfo(@"\Внутренний общий накопитель\Pictures\NewImages\");

          IEnumerable<MediaFileInfo> files = photoDir.EnumerateFiles("*.*", SearchOption.AllDirectories);

          foreach (var file in files)
          {
            if (picturesList.SelectedItem.ToString() == file.Name)
            {
              MemoryStream memoryStream = new System.IO.MemoryStream();
              device.DownloadFile(file.FullName, memoryStream);
              memoryStream.Position = 0;
              WriteSreamToDisk(@"C:\PicturesFromOculus\" + file.Name, memoryStream);
              selectedPicture.Source = null;
              string picturePath = file.FullName;
              BitmapImage logo = new BitmapImage();
              logo.BeginInit();
              logo.UriSource = new Uri($@"C:\PicturesFromOculus\{file.Name}");
              logo.EndInit();
              selectedPicture.Source = logo;
            }
          }
          device.Disconnect();
        }
      }
    }
    private void OnClosed(object sender, EventArgs e)
    {
      selectedPicture.Source = null;
      Process proc = new Process();
      proc.StartInfo.FileName = Directory.GetCurrentDirectory() + @"\deletePictures.bat";
      _ = proc.Start();
    }

  }
}
